Create seperate envionment using virtualenvwrapper  
```
./create_env.sh
```

Install and configure pgsql
```
./pgsql_install.sh
```

Install Geonode 4.1.0 instance in development mode
```
./install.sh
```
Start Geonode
```
./geonode_start.sh
```
Stop geonode
```
./geonode_stop.sh
```


[click here](https://gitlab.com/binoy194/geonode_4.1.0/-/blob/main/geonode-project/README.md?ref_type=heads) for detailed documentation

Other references  
https://tekworldthink.blogspot.com/2023/07/how-to-install-geonode-project-for.html  
https://geonode.org/  
https://github.com/GeoNode/geonode-project  
https://docs.geonode.org/en/master/install/advanced/project/index.html  
